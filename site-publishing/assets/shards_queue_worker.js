var BASE_URL="https://patchwork.freedesktop.org/api/1.0/";

function parse_series_rev(series_rev) {
    var series_id = series_rev.split('v')[0];
    var rev_id = series_rev.split('v')[1];

    return { 'id': series_id, 'rev': rev_id, 'name': series_rev };
}

function query_bat_result(series) {
    var url = BASE_URL + 'series/' + series.id + '/revisions/' + series.rev + '/test-results/';
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, false); // synchronous
    xhttp.setRequestHeader('Accept', 'application/json');
    xhttp.send();
    if (xhttp.status === 200) {
        var json = JSON.parse(xhttp.responseText);

        for (var i = 0; i < json.length; i++) {
            if (json[i]['test_name'] != 'Fi.CI.BAT')
                continue;

            return { 'date': json[i]['date'], 'state': json[i]['state'] };
        }

    }
    throw error;
}

onmessage = function(e) {
    var shards = e.data.split("\n");
    var results = [];

    for (var i = 0; i < shards.length; i++) {
        if (!shards[i])
            continue;
        var series = parse_series_rev(shards[i]);

        try {
            var result = query_bat_result(series);
        } catch (error) {
            // discard if didn't return data
            continue;
        }
        result.series = series;
        results.push(result);
    }

    // no BAT, no good, filter them out
    results = results.filter(function(r) {
        return ('date' in  r);
    });

    // if result is not a success we won't run that shard anyway
    results = results.filter(function(r) {
        return !(r.state == 'failure');
    });

    results.sort(function(a,b) {
        return a.date.localeCompare(b.date);
    });

    postMessage(results);
};
